package terraform

import input.tfplan as tfplan

allowed_platforms = ["aws", "azurerm", "google"]


allowed_instance_types = {
    "aws": ["t5.micro"]
}

res := tfplan.resource_changes

resources := [r | r = res[_]; allowed_platforms[_] == r.provider_name]


contains(arr, elem) {
  arr[_] = elem
}


# Check instance types
deny[msg] {
    r = res[_]
    instance_types = allowed_instance_types[r.provider_name]
    not contains(instance_types, r.change.after.instance_type)
    msg := sprintf("Instance type '%s' is not allowed on cloud '%s'", 
                    [r.change.after.instance_type, r.provider_name])
}
