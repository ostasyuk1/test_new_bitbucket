version = "v1" 


policy "opa1" {
  enabled = true  
  enforcement_level = soft-mandatory
}
policy "opa2" {
  enabled = true  
  enforcement_level = soft-mandatory
}

policy "opahhh" {
  enabled = true  
  enforcement_level = hard-mandatory
}