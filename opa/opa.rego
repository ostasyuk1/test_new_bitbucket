package terraform

import input.tfplan as tfplan

allowed_platforms = ["aws", "azurerm", "google"]


allowed_instance_types = {
    "aws": ["t1.micro", "t1.small", "t1.nano"],
    "google": ["f1-micro", "n1-standard-1", "n1-standard-2"],
    "azurerm": ["Basic_A0", "Basic_A1", "Basic_A2", "Standard_D1_v2", "Standard_DS1_v2", "Standard_DS2_v2"]
}

res := tfplan.resource_changes

resources := [r | r = res[_]; allowed_platforms[_] == r.provider_name]


contains(arr, elem) {
  arr[_] = elem
}


# Check instance types
deny[msg] {
    r = res[_]
    instance_types = allowed_instance_types[r.provider_name]
    not contains(instance_types, r.change.after.instance_type)
    msg := sprintf("Instance type '%s' is not allowed on cloud '%s'", 
                    [r.change.after.instance_type, r.provider_name])
}

